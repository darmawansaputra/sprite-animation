package com.darmawan.actorspire;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

public class GameView extends SurfaceView implements Runnable {

    Thread gameThread = null;
    SurfaceHolder ourHolder;
    volatile boolean playing;

    Canvas canvas;
    Paint paint;

    long fps;
    private long timeThisFrame;
    Bitmap bitmapBob;
    boolean isMoving = false;
    float walkSpeedPerSecond = 250;
    float bobXPosition = 230;
    float bobYPosition = 300;

    private int frameWidth = 128;
    private int frameHeight = 222;

    // How many frames are there on the sprite sheet?
    private int frameCount = 5;

    // Start at the first frame - where else?
    private int currentFrame = 0;

    // What time was it when we last changed frames
    private long lastFrameChangeTime = 0;

    // How long should each frame last
    private int frameLengthInMilliseconds = 100;

    int width;
    boolean flipped = false;

    private Rect frameToDraw = new Rect(
            0,
            0,
            frameWidth,
            frameHeight);

    // A rect that defines an area of the screen
    // on which to draw
    RectF whereToDraw = new RectF(
            bobXPosition,
            bobYPosition,
            bobXPosition + frameWidth,
            bobYPosition + frameHeight);

    public GameView(Context context) {
        super(context);

        ourHolder = getHolder();
        paint = new Paint();
        bitmapBob = BitmapFactory.decodeResource(this.getResources(), R.drawable.bob);
        playing = true;

        bitmapBob = Bitmap.createScaledBitmap(bitmapBob,
                frameWidth * frameCount,
                frameHeight,
                false);

        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        width = display.getWidth() - bitmapBob.getWidth() / frameCount;
    }

    @Override
    public void run() {
        while (playing) {
            long startFrameTime = System.currentTimeMillis();

            update();
            draw();

            timeThisFrame = System.currentTimeMillis() - startFrameTime;

            if (timeThisFrame > 0) {
                fps = 1000 / timeThisFrame;
            }
        }
    }

    public void update() {
        if (isMoving) {
            bobXPosition += walkSpeedPerSecond / fps;

//            Log.d("GameView", String.valueOf(bobXPosition));

            if (bobXPosition > width || bobXPosition < 0) {
                walkSpeedPerSecond = -walkSpeedPerSecond;
                flipped = !flipped;
            }
        }
    }

    public void getCurrentFrame(){

        long time  = System.currentTimeMillis();
        if(isMoving) {// Only animate if bob is moving
            if ( time > lastFrameChangeTime + frameLengthInMilliseconds) {
                lastFrameChangeTime = time;
                currentFrame++;
                if (currentFrame >= frameCount) {

                    currentFrame = 0;
                }
            }
        }
        //update the left and right values of the source of
        //the next frame on the spritesheet
        frameToDraw.left = currentFrame * frameWidth;
        frameToDraw.right = frameToDraw.left + frameWidth;

    }

    public void draw() {
        if (ourHolder.getSurface().isValid()) {
            canvas = ourHolder.lockCanvas();
            canvas.drawColor(Color.argb(255, 255,255,255));
            paint.setColor(Color.argb(255,0,0,0));
            paint.setTextSize(65);
            canvas.drawText("FPS: " + fps, width / 2,110, paint);

            whereToDraw.set((int)bobXPosition,
                    bobYPosition,
                    (int)bobXPosition + frameWidth,
                    bobYPosition + frameHeight);

            getCurrentFrame();

            Matrix matrix = new Matrix();
            matrix.preScale(flipped ? -1.0f : 1.0f, 1.0f);

            Bitmap rotatedBitmap = Bitmap.createBitmap(bitmapBob, 0, 0, bitmapBob.getWidth(),
                    bitmapBob.getHeight(), matrix, true);

            canvas.drawBitmap(rotatedBitmap,
                    frameToDraw,
                    whereToDraw, paint);

            ourHolder.unlockCanvasAndPost(canvas);
        }
    }

    public void pause() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.e("GameView Error: ", "joining thread");
        }
    }

    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                isMoving = true;
                break;
            case MotionEvent.ACTION_UP:
                isMoving = false;
                break;
        }

        return true;
    }
}
